# Install Arch Linux with full disk encryption using UEFI.

## Create Installation Media
```
dd if=PATH_OF_ISO of=/dev/sd_ status=progress && sync
```

## Boot into USB
### Find install device
```
lsblk
```

### Optional: Secure wipe by writing random bytes to disk
```
dd if=/dev/urandom of=/dev/nvme0n1 status=progress && sync
```

### Create partitions
```
cfdisk /dev/nvme0n1
```

Create EFI partition size: 512M Hex code: ef00
Create new root partition size: rest of disk,  Hex code: 8300

### Create EFI partition
```
mkfs.vfat -F32 -n EFI /dev/nvme0n1p1
```

## Setup Encryption

```
cryptsetup --use-random luksFormat /dev/nvme0n1p2
cryptsetup luksOpen /dev/nvm0n1p2 luks
```
### Create encrypted partitions
```
pvcreate /dev/mapper/luks
vgcreate vg0 /dev/mapper/luks
lvcreate --size 16G vg0 --name swap
lvcreate -l +100%FREE vg0 --name root
```

### Create filesystems on encrypted partitions
```
mkfs.ext4 -L root /dev/mapper/vg0-root
mkswap /dev/mapper/vg0-swap
```

## Mount
```
mount /dev/mapper/vg0-root /mnt # /mnt is the installed system
swapon /dev/mapper/vg0-swap # Not needed but a good thing to test
mkdir /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot
```


### Install base system
```
$pacstrap /mnt base base-devel git sudo efibootmgr dialog wpa_supplicant intel-ucode linux-lts linux-firmware mkinitcpio lvm2
```

### Generate fstab
```
$genfstab -pU /mnt | tee -a >> /mnt/etc/fstab
```

### Change root to configure base system
```
arch-chroot /mnt
```

### Set locale
```
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
```

### Set timezone (you may need to $rm /etc/localtime)
```
ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
hwclock --systohc --utc
```

### Set hostname
```
echo HOSTNAME > /etc/hostname
```

### Set root password
```
passwd
```

### Install packages needed for wifi
```
pacman -S wpa_supplicant dialog netctl dhcpcd
```

## Configure mkinitcpio
```
nano /etc/mkinitcpio.conf
```
Add `ext4` to MODULES
Add `encrypt`, `lvm2` and `resume` to hooks right before filesystems
```
HOOKS=(base udev autodetect modconf block encrpt lvm2 resume filesystems keyboard fsck)
```

### Regenerate initrd image
```
mkinitcpio -p [KERNAL_NAME]
```
Usually kernal is `linux` but I am using `linux-lts`

### Setup systembootd
```
bootctl --path=/boot install
```

### Create loader.conf
```
echo default arch >> /boot/loader/loader.conf
echo timeout 5 >> /boot/loader/loader.conf
```

### Create arch.conf
```
nano /boot/loader/entries/arch.conf
```
Add the following:
```
title Arch Linux
linux /vmlinuz-linux-lts
initrd /intel-ucode.img
initrd /initramfs-linux-lts.img
options cryptdevice=UUID=<UUID>:vg0 root=/dev/mapper/vg0-root resume=/dev/mapper/vg0-swap rw intel_pstate=no_hwp
```
`<UUID>` is the the one of the raw encrypted device (/dev/nvme0n1p2). It can be found with the `blkid` command
`vmlinuz-linux-lts` and `initramfs-linux-lts` will be different based on kernal used, example for lts shown

### Exit root unmount partitions and reboot
```
exit
umount -R /mnt/boot
umount -R /mnt
reboot
```