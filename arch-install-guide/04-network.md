# Setup network

## Install
```
sudo pacman -S networkmanager network-manager-applet
```

## Disable services
```
ip link
sudo systemctl disable dhcpcd@enpxxx.service
sudo systemctl disable netctl-auto@wxpxsx.service
```

## Enable network manager
```
sudo systemctl enable NetworkManager.service
```

## VPN
```
sudo pacman -S openvpn easy-rsa networkmanager-openvpn
```

## Check DNS Servers
```
cat /etc/resolv.conf
```