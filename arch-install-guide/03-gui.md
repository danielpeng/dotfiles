## Install Terminal Emulator
```
sudo pacman -S termite
```

## Install x server
```
sudo pacman -S xorg xorg-xinit
cp /etc/X11/xinit/xinitrc ~/.xinitrc
```

## Install i3, i3status, i3lock and rofi
```
sudo pacman -S i3-wm i3status i3lock rofi
```
Add `exec i3` to .xinitrc

## Start UI
```
startx
```