# Additional Setup

## Create User
```
useradd USERNAME -m -g users -G wheel
```

## Disable password check on sudo
```
export EDITOR=vim
visudo
```
Append to end of file: `USERNAME ALL = NOPASSWD: ALL`

## Setup AUR
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```
Some packages might fail due to size limits to /tmp.
```
mount -o remount, size=8G /tmp/
```

## Sync Time
```
timedatectl set-ntp true
```
